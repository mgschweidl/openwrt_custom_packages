#
# Copyright (C) 2007-2011 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=wireshark-2.6
PKG_VERSION:=2.6.1
PKG_RELEASE:=1

PKG_SOURCE:=wireshark-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://github.com/wireshark/wireshark/archive/
PKG_HASH:=89777fe21da7f1b4616c6e9a750f6d06a9424b438059be1e41c37d13d6d8a626

PKG_LICENSE:=MIT
PKG_LICENSE_FILES:=COPYING

PKG_FIXUP:=autoreconf

include $(INCLUDE_DIR)/package.mk

TAR_CMD=$(HOST_TAR) -C $(1) --strip-components 1 $(TAR_OPTIONS)

define Package/wireshark-2.6
	SECTION:=net
	CATEGORY:=Network
	TITLE:=Network monitoring and data tool
	URL:=http://www.wireshark.org/
	MAINTAINER:=Manfred Gschweidl <m.gschweidl@gmail.com>
	DEPENDS:=+librt +libcap +glib2 +tcpdump +libc +libcares +libgcrypt +libgnutls +libxml2 +liblz4
endef

CONFIGURE_ARGS+= \
	--enable-tshark \
	--enable-dumpcap \
	--enable-setuid-install \
 	--disable-gtk2 \
 	--disable-androiddump \
	--disable-randpktdump \
	--disable-ipv6 \
	--without-lua \
	--disable-glibtest \
	--without-plugins \
	--disable-wireshark \
	--disable-gtktest \
	--disable-editcap \
	--disable-capinfos \
	--disable-mergecap \
	--disable-text2pcap \
	--disable-idl2wrs \
	--disable-dftest \
	--disable-randpkt \
	--with-c-ares \
	--with-gcrypt \
	--with-gnutls \
	--with-libxml2=yes \
	--with-lz4 \
	--disable-udpdump

TARGET_CFLAGS += -ffunction-sections -fdata-sections
TARGET_LDFLAGS += -Wl,--gc-sections

CONFIGURE_VARS += \
	BUILD_CC="$(TARGET_CC)" \
	HOSTCC="$(HOSTCC)" \
	td_cv_buggygetaddrinfo=no \
	ac_cv_linux_vers=$(LINUX_VERSION) \
	ac_cv_header_rpc_rpcent_h=no \
	ac_cv_lib_rpc_main=no \
	ac_cv_path_PCAP_CONFIG=""

MAKE_FLAGS += \
	CCOPT="$(TARGET_CFLAGS)" INCLS="-I. $(TARGET_CPPFLAGS)"

define Build/Compile
	$(LN) -sf $(STAGING_DIR_ROOT)/lib/ld* $(STAGING_DIR)/lib/loader
	$(LN) -sf $(STAGING_DIR_ROOT)/lib/libgcc*.so* $(STAGING_DIR)/lib/
	cd $(PKG_BUILD_DIR)/tools/lemon&&make
	$(MAKE) $(PKG_JOBS) -C $(PKG_BUILD_DIR) \
		DESTDIR="$(PKG_INSTALL_DIR)" \
		CC="$(TARGET_CC)" \
		install
	rm -f $(STAGING_DIR)/lib/loader
	rm -f $(STAGING_DIR)/lib/libgcc*.so*
endef

define Package/wireshark-2.6/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/tshark $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/captype $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/dumpcap $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/rawshark $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/reordercap $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/sharkd $(1)/usr/bin/
	$(INSTALL_DIR) $(1)/usr/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/lib*.so* $(1)/usr/lib
#	$(CP) $(PKG_INSTALL_DIR)/usr/lib/wireshark/plugins/$(PKG_VERSION)/*.so $(1)/usr/lib
endef

$(eval $(call BuildPackage,wireshark-2.6))
